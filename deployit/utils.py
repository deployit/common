import logging

def str_to_loglevel(loglevel_string):
    """Translate string to logging.<LOGLEVEL>"""

    if loglevel_string is None:
        return logging.WARNING

    loglevel_upper = loglevel_string.upper()

    if hasattr(logging, loglevel_upper):
        return getattr(logging, loglevel_upper)
    else:
        return logging.WARNING
