"""Message Class """

import json
import uuid
import datetime
import os
import pytz

class MessageError(Exception):
    """Generic Message Error

    Do not instantiate directly. Use subclasses.

    """
    def __init__(self, msg="Generic Message Error"):
        super(MessageError, self).__init__(msg)


class MessageInitializeError(MessageError):
    """Message initialization Error.

    Raised if it has been tried to initialize a Message from an
    invalid type or JSON

    """
    def __init__(self, msg):
        super(MessageInitializeError, self).__init__(msg)


class _MessageHeader(dict):
    """A message header

    Some keys are immutable, e.g. 'uuid', 'host'.

    """

    SERIALIZATION = 'serialized'
    DESERIALIZATION = 'deserialized'
    # header keys that cannot be deleted or changed
    immutable_header_keys = [ 'uuid', 'created-by', 'created-on', 'serialized-on', 'deserialized-on' ]

    def __init__(self, *args):
        """Initialize Message Header

        If dict is None, uuid, created-on, and create-by will be set
        automatically, else they will be taken from dict if existing.

        """
        super(_MessageHeader, self).__init__(*args)
        
        super(_MessageHeader, self).setdefault('uuid', str(uuid.uuid4()))
        super(_MessageHeader, self).setdefault('created-by', os.uname()[1])
        super(_MessageHeader, self).setdefault('created-on', datetime.datetime.now(pytz.timezone('UTC')).isoformat())

    def __setitem__(self, key, val):
        self._immutable_key(key)
        super(_MessageHeader, self).__setitem__(key, val)

    def __delitem__(self, key):
        self._immutable_key(key)
        super(_MessageHeader, self).__delitem__(key)

    def clear(self):
        _save_immutable_keys()
        super(_MessageHeader, self).clear()
        _restore_immutable_keys()

    def pop(self, key, val=None):
        self._immutable_key(key)
        return super(_MessageHeader, self).pop(key, val)

    def update(self, dict=None, **kwargs):
        _save_immutable_keys()
        super(_MessageHeader, self).update(dict, **kwargs)
        _restore_immutable_keys()

    def setdefault(self, name, val=None):
        self._immutable_key()
        return super(_MessageHeader, self).setdefault(name, val)

    def update_serialization(self, type):
        if not type + '-on' in self:
            super(_MessageHeader, self).__setitem__(type + '-on', [self._create_host_date_dict()])
        else:
            self[type + '-on'].append(self._create_host_date_dict())

    def _create_host_date_dict(self):
        return {
                'host': os.uname()[1],
                'date': datetime.datetime.now(pytz.timezone('UTC')).isoformat()
        }

    def _save_immutable_keys(self):
        self.__backup_dict = dict()
        for k in immutable_header_keys:
            self.__backup_dict[k] = self[k]

    def _restore_immutable_keys(self):
        assert self.__backup_dict is not None
        super(_MessageHeader, self).update(**self.__backup_dict)

    def _immutable_key(self, key):
        if key in _MessageHeader.immutable_header_keys:
            raise KeyError("'%s' may not be altered" % key)


class Message(object):
    """Represent a Message that will be sent over the wire using Rabbit

    A message has always a header and a body part (which can be None).

    Every Message object has at least the following header keys:

     * uuid: uuid of the message

     * created-at: when the message has been created

     * created-on: on which host the message has been created

    """
    def _init_from_json(self, msgobj):
        """Initialize object from JSON.

        Does not create a new uuid.

        """

        try:
            _dict = json.loads(msgobj)
        except Exception as ex:
            raise MessageInitializeError("Unable to load from JSON: %s" % str(ex))

        if 'header' not in _dict:
            raise MessageInitializeError("'header' is missing in JSON")
        if 'body' not in _dict:
            raise MessageInitializeError("'body' is missing in JSON")

        self.header = _MessageHeader(_dict['header'])
        self.header.update_serialization(_MessageHeader.DESERIALIZATION)

        self.body = _dict['body']

    def __init__(self, messageobj=None):
        """Create message object.

        If :param messageobj: is None, a new Message instance will
        created with a new uuid. If it is a string, the string will be
        treated as json. If it is another Message, references to the
        header and body will be copied to the new object.

        In the last two cases, no new uuid will be generated for the
        message.

        """

        if messageobj is not None:
            if isinstance(messageobj, Message):
                self.header = messageobj.header
                self.body = messageobj.body
                return
            elif isinstance(messageobj, str):
                self._init_from_json(messageobj)
                return
            else:
                raise MessageInitializeError("Message can only be initialized from Message or JSON")

        # Initialize default header
        self.header = _MessageHeader()
        self.body = None

    def __str__(self):
        self.header.update_serialization(_MessageHeader.SERIALIZATION)
        return json.dumps({ 'header': self.header, 'body': self.body })
