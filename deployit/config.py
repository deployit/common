"""Indirection for configuration file handling"""

import ConfigParser

database = None
rabbit = None
modules = None
agent = None
listener = None

__default_config__ = {
    'database' : {
        'path': '/tmp/deployit-database.sqlite'
    },
    'rabbit': {
        'host': 'localhost',
        'vhost': 'test',
        'port': 5672,
        'user': 'guest',
        'password': 'guest',
        'ssl_cacertfile': None,
        'ssl_keyfile': None,
        'ssl_certfile': None
    },
    'modules': {
        'path': '/etc/deployit/modules'
    },
    'agent': {
        'pidfile': '/var/run/deployitagent.pid',
        'loglevel': 'warning',
        'reconnect_interval': 2
    },
    'listener': {
        'pidfile': '/var/run/deployitlistener.pid',
        'loglevel': 'warning',
        'reconnect_interval': 2
    }
}

def read(path):
    global database
    global rabbit
    global modules
    global agent
    global listener

    database = __default_config__['database']
    rabbit = __default_config__['rabbit']
    modules = __default_config__['modules']
    agent = __default_config__['agent']
    listener = __default_config__['listener']
    
    cparser = ConfigParser.ConfigParser()
    cparser.read(path)

    try:
        database.update(cparser.items('database'))
    except:
        pass

    try:
        rabbit.update(cparser.items('rabbit'))
    except:
        pass

    try:
        modules.update(cparser.items('modules'))
    except:
        pass

    try:
        agent.update(cparser.items('agent'))
    except:
        pass

    try:
        listener.update(cparser.items('listener'))
    except:
        pass

    # fix the type of port in rabbit
    rabbit['port'] = int(rabbit['port'])
    agent['reconnect_interval'] = int(agent['reconnect_interval'])
    listener['reconnect_interval'] = int(listener['reconnect_interval'])
