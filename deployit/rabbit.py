"""Provide interfaces to RabbitMQ

Provide an interface o RabbitMQ suitable for both the Management Host
and Management Agent

"""

import deployit.config
import deployit.deployitlogging
import uuid
import os
import datetime
import json
import deployitlogging
import pika
import pytz
import ssl
import logging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

# Message has not been accepted
MSG_NAK = 0

# Message has been accepted
MSG_ACK = 1

# The reply exchange used to inform the management host
MANAGEMENTHOST_EXCHANGE = "managementhost-status-reply"

# The message content type
MSG_CONTENT_TYPE = 'application/json'


class NoExchange(Exception):
    """Exchange does not exist in list"""
    def __init__(self, name):
        super(NoExchange, self).__init__()
        self.ex_name = name

    def __str__(self):
        return "Exchange '%s' does not exist" % self.ex_name


class NotConnected(Exception):
    """Not connected to RabbitMQ"""
    def __str__(self):
        return "Not connected to RabbitMQ"


class Rabbit(object):
    """Rabbit class

    TODO: cleanup redundant code consume_from_(fanout|direct)
    publish_to_(fanout|direct)

    """
    def __init__(self):
        """Initialize connection to Rabbit server.

        Before creating Rabbit objects,
        deployit.config.initialize_rabbit() must be called.

        """

        ssl_options = { 'keyfile': deployit.config.rabbit['ssl_keyfile'],
                        'certfile': deployit.config.rabbit['ssl_certfile'],
                        'ca_certs': deployit.config.rabbit['ssl_cacertfile'],
                        'cert_reqs': ssl.CERT_REQUIRED,
                        'ssl_version': ssl.PROTOCOL_TLSv1
                    }

        credentials = pika.PlainCredentials(deployit.config.rabbit['user'],
                                            deployit.config.rabbit['password'])
        parameters = pika.ConnectionParameters(host=deployit.config.rabbit['host'],
                                               port=deployit.config.rabbit['port'],
                                               virtual_host=deployit.config.rabbit['vhost'],
                                               credentials=credentials,
                                               ssl=True,
                                               ssl_options=ssl_options)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

        self._consuming = False

        # Caches
        self.exchange_list = list()
        self.queue_list = list()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def declare_exchange(self, resourcename, fanout):
        exchange_name = _generate_exchange_name(resourcename)

        if exchange_name in self.exchange_list:
            logger.debug("Not declaring exchange '%s': already declared", exchange_name)
            return

        if fanout:
            exchange_type = 'fanout'
        else:
            exchange_type = 'direct'
        self.channel.exchange_declare(exchange=exchange_name,
                                      exchange_type=exchange_type,
                                      passive=False,
                                      durable=True,
                                      auto_delete=False)

        self.exchange_list.append(exchange_name)

    def consume_from_fanout(self, resourcename, callback):
        self._consume(resourcename, True, callback)

    def consume_from_direct(self, resourcename, callback):
        self._consume(resourcename, False, callback)

    def publish_to_fanout(self, resourcename, message):
        return self._publish(resourcename, True, message)

    def publish_to_direct(self, resourcename, message):
        return self._publish(resourcename, False, message)

    def bind_queue(self, resourcename, fanout):
        exchange_name = _generate_exchange_name(resourcename)
        queue_name = _generate_queue_name(resourcename)

        self.declare_exchange(resourcename, fanout)

        if queue_name in self.queue_list:
            logger.debug("Not binding queue '%s' to exchange '%s': already bound.", queue_name, exchange_name)
            return

        self.channel.queue_declare(queue=queue_name)
        self.channel.queue_bind(queue=queue_name,
                                exchange=exchange_name,
                                routing_key="")

        self.queue_list.append(queue_name)

    def get_msg(self, resourcename):
        """
        Get one message from the queue. If the message has not the proper
        type, it will be rejected.

        The function return a tuple (status, reason, body)

        This method requires bind_queue() to be called prior.

        """
        method, header, body = self.channel.basic_get(
            queue=_generate_queue_name(resourcename))

        if header.content_type != MSG_CONTENT_TYPE:
            self.channel.basic_nack(delivery_tag=method.delivery_tag,
                                    requeue=False)
            reason="Unexpected content type '%s'. Expected '%s'" % \
                (header.content_type, MSG_CONTENT_TYPE)
            return (MSG_NAK, reason, None)

        self.channel.basic_ack(delivery_tag=method.delivery_tag)
        return (MSG_ACK, "Ok", body)

    def start_consuming(self):
        self.channel.start_consuming()
        self._consuming = True

    def stop_consuming(self):
        try:
            self.channel.stop_consuming()
        except Exception as e:
            logger.error("While stopping consuming: %s", repr(e))
        self._consuming = False

    def close(self):
        try:
            logger.debug("close(): stop consuming")
            if self._consuming:
                self.channel.stop_consuming()
        except Exception as ex:
            logger.warning("Exception on channel.stop_consuming(): %s", str(ex))

        try:
            logger.debug("close(): channel close")
            self.channel.close()
        except Exception as ex:
            logger.warning("Exception on channel.close(): %s", str(ex))

        try:
            logger.debug("close(): connection close")
            self.connection.close()
        except Exception as ex:
            logger.warning("Exception on connection.close(): %s", str(ex))

    def _consume(self, resourcename, fanout, callback):
        queue_name = _generate_queue_name(resourcename)

        self.bind_queue(resourcename, fanout)
        self.channel.basic_consume(callback, queue_name)

    def _publish(self, resourcename, fanout, message):
        """Publish a message

        :param resourcename: the resource publishing the message

        :param fanout: whether or not declare the exchange as exchange_type
        fanout. This only takes effect on the first call of this
        method. Subsequent calls on the same instance won't declare
        the exchange.

        :param message: the message as Message object.

        :param supplemental_header: dictionary with supplemental
        headers, or None. It must be serializable using json.

        """

        msg_props = pika.BasicProperties()
        msg_props.content_type = MSG_CONTENT_TYPE

        exchange_name = _generate_exchange_name(resourcename)
        self.declare_exchange(resourcename, fanout)

        self.channel.basic_publish(body=str(message),
                                   exchange=exchange_name,
                                   routing_key="",
                                   properties=msg_props)


def _generate_consumer_tag(name):
    return name.lower() + "-consumer"

def _generate_queue_name(name):
    """Generate internal queue name"""
    return name.lower() + "-queue-" + os.uname()[1]

def _generate_exchange_name(module_name):
    """Generate internal exchange name"""
    return module_name.lower() + '-exchange'
