"""Module functions and classes common to both, the Management Host
and Management Agent.

"""

import glob
import os
import imp
import deployitlogging
import deployit.message
import deployit.deployitlogging
import logging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

modulelist = dict()
resourcelist = dict()

class Module(object):
    """Most basic module possible.

    Has only a name, resource name, and version. This class is not
    intended to be used on its own, rather than to be inherited from.

    Further do not instantiate inherited class directly. It must be
    instantiated by modules.init_modules(), since it requires global
    structures to be set up to work.

    """
    def __init__(self, name="NoName",
                 resourcename="noname",
                 version="NoVersion"):
        """Initialize the Module meta information.

        :param name: the name of the module

        :param resourcename: name of the resource this module handles

        :param version: version of the module. Must be integer

        """
        self.name = name
        self.resourcename = resourcename
        if type(version) is not int:
            raise TypeError("version must be integer")

        self.version = version

    def message_template(self):
        """Factor a new deployit.Message object

        Return a deployit.Message object suitable for sending via
        rabbit, with header preloaded with:

        """
        msg_template = deployit.Message()
        msg_template.header['message-creator'] = { 'module':
                                                   self.name,
                                                   'resource':
                                                   self.resourcename,
                                                   'version':
                                                   self.version,
                                                   'modulepath':
                                                   deployit.modules.modulelist[self.name]['path']
        }
        return msg_template

    def identification(self):

        """Return module identification.

        Return a tuple (name, resourcename, server, instance)
        comprising the module identification.

        The instance will be used to communicate with the module.

        """
        return (self.name, self.resourcename, self.version, self)


def init_modules(path):
    """Load and initialize modules from a directory.

    Read the files ending in .py in the specified directory or list of
    directories and calls the initialize() method of each module.

    The modules are python files that are loaded. For each file
    loaded, the function calls the modules initialize() function. This
    function has to return the module identification as provided by
    Module.identification().

    A trivial module would look like this

    import deployit.modules

    class MyModule(deployit.Module):
        def __init__(self):
            super(MyModule, self).__init__('MyModule', 'mymodule', '0.1')


    def initialize():
        return MyModule().identification()


    init_modules() will create two dictionaries of loaded modules:

       deployit.modules.modulelist

    and

       deployit.modules.resourcelist


    deployit.modules.modulelist uses the module name as
    key. deployit.modules.resourcelist uses the resource name as key.

    Each entry in the deployit.modules.modulelist and
    deployit.modules.resourcelist is dict like this

    {
      'module': 'module name',
      'resource': 'resource name',
      'version': 'version',
      'instance': ModuleInstance,
      'path': 'path to python module file'
    }

    """
    global modulelist
    global resourcelist

    _path = path
    if type(_path) is str:
        _path = (_path,) # make tuple, so we can handle like list

    for path in _path:
        logger.debug("Try to find modules in '%s'", path)
        for module_file in glob.glob(os.path.join(path, '*.py')):
            logger.debug("Process module file '%s'", module_file)
            modulename = os.path.splitext(os.path.basename(module_file))[0]
            module = imp.load_source(modulename, os.path.abspath(module_file))

            mname, resourcename, version, instance = module.initialize()
            logger.info("Initialized Module %s (%s) from %s",
                        mname, version, os.path.abspath(module_file))

            # if module name has been defined, skip this module
            if mname in modulelist:
                logger.warning("Module '%s' defined in %s already initialized from %s. Skipping",
                                mname, module_file,
                                modulelist[mname]['path'])
                continue

            modulelist[mname] = {
                'module': mname,
                'resource': resourcename,
                'version': version,
                'instance': instance,
                'path': os.path.abspath(module_file)
            }

            # if the resourcelist already has such a key, overwrite it
            if resourcename in resourcelist:
                logger.warning("Resource '%s' of Module '%s' loaded from %s overwritten by resource defined in '%s'",
                                resourcename,
                                mname,
                                resourcelist[resourcename]['path'],
                                module_file)

            # link the resource name to the entry in the module list
            resourcelist[resourcename] = modulelist[mname]
