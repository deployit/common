from distutils.core import setup
setup(name='deployit',
      version='0.2',
      description='Common DeployIT modules',
      author='Rafael Ostertag',
      author_email='support@math.uzh.ch',
      packages=['deployit'])
