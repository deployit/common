""" Dummy Module doing nothing """

import deployit.modules
import web
import uuid
import json

class Dummy2(deployit.Module):
    def __init__(self):
        super(Dummy2, self).__init__('Dummy2', 'dummy2', 1)

def initialize():
    return Dummy2().identification()
