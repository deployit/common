""" Dummy Module doing nothing """

import deployit.modules

class Dummy(deployit.Module):
    def __init__(self):
        super(Dummy, self).__init__('Dummy', 'dummy', 1)

def initialize():
    return Dummy().identification()
