#!/usr/bin/env python

import unittest
import pika
import deployit.config
import deployit.rabbit
import deployit.message
import json

class TestRabbit(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        deployit.config.read('testconfig.cfg')

    def test_send_message(self):
        with deployit.Rabbit() as producer, deployit.Rabbit() as consumer:
            consumer.bind_queue('fanout-test', True)

            message = deployit.Message()
            message.body={ 'msg': 'Hello, there' }
            producer.publish_to_fanout('fanout-test', message)

            status, reason, body = consumer.get_msg('fanout-test')

        self.assertEqual(status, deployit.rabbit.MSG_ACK)
        self.assertIsNotNone(body)

        msg = deployit.Message(body)
        self.assertEqual('Hello, there', msg.body['msg'])


if __name__ == '__main__':
    unittest.main()
