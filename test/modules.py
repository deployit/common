#!/usr/bin/env python

import unittest
import deployit.modules
import deployit.config

class TestModules(unittest.TestCase):
    """
    Test proper initialization of modules
    """

    @classmethod
    def setUpClass(cls):
        """Initialize modules from the fixtures/ directory

        """
        deployit.config.read('testconfig.cfg')
        deployit.modules.init_modules(deployit.config.modules['path'])

    def test_module_existence(self):
        """Test whether modules have been loaded

        """
        self.assertIn('Dummy', deployit.modules.modulelist)
        self.assertIn('dummy', deployit.modules.resourcelist)
        self.assertIn('Dummy2', deployit.modules.modulelist)
        self.assertIn('dummy2', deployit.modules.resourcelist)

    def test_module_equivalence(self):
        """Test whether modulelist and resourcelist are identical

        """
        self.assertEqual(len(deployit.modules.modulelist), 2)
        self.assertEqual(len(deployit.modules.resourcelist), 2)
        self.assertEqual(deployit.modules.modulelist['Dummy'],deployit.modules.resourcelist['dummy'])
        self.assertEqual(deployit.modules.modulelist['Dummy2'],deployit.modules.resourcelist['dummy2'])
        
    def test_module_modulelist_content(self):
        """Test if modules have been properly initialized

        """
        self.assertEqual(deployit.modules.modulelist['Dummy']['version'],1)
        self.assertEqual(deployit.modules.modulelist['Dummy']['module'],'Dummy')
        self.assertEqual(deployit.modules.modulelist['Dummy']['resource'],'dummy')
        self.assertEqual(deployit.modules.modulelist['Dummy']['path'],'fixtures/dummy.py')
        self.assertEqual(deployit.modules.modulelist['Dummy2']['version'],1)
        self.assertEqual(deployit.modules.modulelist['Dummy2']['module'],'Dummy2')
        self.assertEqual(deployit.modules.modulelist['Dummy2']['resource'],'dummy2')
        self.assertEqual(deployit.modules.modulelist['Dummy2']['path'],'fixtures/dummy2.py')


if __name__ == '__main__':
    unittest.main()
