#!/usr/bin/env python

import unittest
import deployit.message
import json
import logging

class TestMessage(unittest.TestCase):
    def test_erroneous_intialization(self):
        """Test behavior when using wrong types to intialize"""
        with self.assertRaises(deployit.message.MessageInitializeError):
            deployit.Message('abc')

        with self.assertRaises(deployit.message.MessageInitializeError):
            deployit.Message(1)

        with self.assertRaises(deployit.message.MessageInitializeError):
            deployit.Message(json.dumps({ 'body': None }))

        with self.assertRaises(deployit.message.MessageInitializeError):
            deployit.Message(json.dumps({ 'header': None }))

    def test_new_message(self):
        """Test creation of new message"""
        newmsg = deployit.Message()
        self.assertIn('uuid', newmsg.header)
        self.assertIn('created-on', newmsg.header)
        self.assertIn('created-by', newmsg.header)
        self.assertIsNone(newmsg.body)

    def test_initialize_with_message(self):
        """Test initialization with another Message"""
        newmsg1 = deployit.Message()
        newmsg2 = deployit.Message(newmsg1)

        self.assertIs(newmsg1.header, newmsg2.header)
        self.assertIs(newmsg1.header['uuid'], newmsg2.header['uuid'])
        self.assertIs(newmsg1.header['created-on'], newmsg2.header['created-on'])
        self.assertIs(newmsg1.header['created-by'], newmsg2.header['created-by'])
        self.assertNotIn('serialized-on', newmsg1.header)
        self.assertNotIn('deserialized-on', newmsg1.header)
        self.assertIs(newmsg1.body, newmsg2.body)
        self.assertIsNone(newmsg1.body)

    def test_initialize_with_json(self):
        """Test initialization with another Message"""
        newmsg1 = deployit.Message()
        newmsg2 = deployit.Message(str(newmsg1))

        self.assertIn('serialized-on', newmsg1.header)
        self.assertIn('deserialized-on', newmsg2.header)

        self.assertIsNone(newmsg1.body)
        self.assertIsNone(newmsg2.body)

    def test_serialize_info(self):
        """Test if list of serialization is kept"""
        newmsg1 = deployit.Message()
        str(newmsg1)
        str(newmsg1)

        self.assertIn('serialized-on', newmsg1.header)
        self.assertEqual(len(newmsg1.header['serialized-on']), 2)
        self.assertNotIn('deserialized-on', newmsg1.header)

    def test_deserialize_info(self):
        """Test if list of deserialize is kept"""
        newmsg1 = deployit.Message()
        newmsg2 = deployit.Message(str(newmsg1))
        newmsg3 = deployit.Message(str(newmsg2))

        self.assertIn('serialized-on', newmsg1.header)
        self.assertNotIn('deserialized-on', newmsg1.header)
        self.assertEqual(len(newmsg1.header['serialized-on']), 1)

        self.assertIn('serialized-on', newmsg2.header)
        self.assertEqual(len(newmsg2.header['serialized-on']), 2)
        self.assertIn('deserialized-on', newmsg2.header)
        self.assertEqual(len(newmsg2.header['deserialized-on']), 1)


        self.assertIn('serialized-on', newmsg3.header)
        self.assertEqual(len(newmsg3.header['serialized-on']), 2)

        self.assertIn('deserialized-on', newmsg3.header)
        self.assertEqual(len(newmsg3.header['deserialized-on']), 2)

    def test_immutable_header_keys(self):
        """Test if immutable header fields are respected"""
        # Create a message with both, serialized-on and
        # deserialized-on fields in the header
        newmsg1 = deployit.Message(str(deployit.Message()))

        for immutable in deployit.message._MessageHeader.immutable_header_keys:
            with self.assertRaises(KeyError):
                newmsg1.header[immutable, 'abc']

            with self.assertRaises(KeyError):
                del newmsg1.header[immutable]

    def test_user_header_keys(self):
        """Test addition/deletion of user header fields"""
        newmsg1 = deployit.Message()
        newmsg2 = deployit.Message(newmsg1)

        newmsg2.header['x-name'] = 'rafi'

        self.assertIn('x-name', newmsg1.header)
        self.assertIn('x-name', newmsg2.header)

        self.assertEqual(newmsg1.header['x-name'], 'rafi')
        self.assertEqual(newmsg1.header['x-name'], newmsg2.header['x-name'])

        del newmsg2.header['x-name']

        self.assertNotIn('x-name', newmsg1.header)
        self.assertNotIn('x-name', newmsg2.header)


if __name__ == '__main__':
    unittest.main()
